import faster_than_requests as requests
import faster_than_csv as csv
import re

f = csv.csv2dict('./list.csv')
for line in f:
    if line[0] != '#':
        entry = line.split(',')
        if entry[3] == 'github':
            text = requests.get2str('https://api.github.com/repos/{link}/releases/latest'.format(link=entry[4]))
            entry[4] = re.search(r'browser_download_url": "(.*AppImage)', text).group(1)
            entry[5] = re.search(r'"name":"(.*?)"', text).group(1)
        elif entry[3] == 'gitlab':
            text = requests.get2str('https://gitlab.com/api/v4/projects/{link}/releases/').format(link=entry[4]))
            entry[4] = re,search(r'', text)
            entry[5] = re,search(r'', text)
